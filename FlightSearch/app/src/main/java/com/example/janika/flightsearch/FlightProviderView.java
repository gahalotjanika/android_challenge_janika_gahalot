package com.example.janika.flightsearch;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

/**
 * @author janika on 07/08/17.
 */

public class FlightProviderView extends RelativeLayout {

    private TextView bookButton;
    private TextView fareText;
    private TextView providerText;



    public FlightProviderView(Context context) {
        super(context);
        init(context, null);
    }

    public FlightProviderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FlightProviderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.flight_provider_view, this, true);

        bookButton = (TextView) findViewById(R.id.book_button);
        fareText = (TextView) findViewById(R.id.fares);
        providerText = (TextView) findViewById(R.id.provider);

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setColor(Color.parseColor("#8a656c"));
        drawable.setCornerRadius((int) Math.ceil(5));
        bookButton.setBackground(drawable);

    }

    public void fillView(JSONObject object){
        bookButton.setText("BOOK");
        fareText.setText(object.optString("fare"));
        switch (object.optInt("providerId")){
            case 1:
                providerText.setText("MakeMyTrip");
                break;
            case 2:
                providerText.setText("Cleartrip");
                break;
            case 3:
                providerText.setText("Yatra");
                break;
            case 4:
                providerText.setText("Musafir");
                break;
        }
    }
}
