package com.example.janika.flightsearch;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author janika on 07/08/17.
 */

public class FlightSearchFragment extends Fragment {

    View mView;
    RecyclerView mRecyclerView;
    RecyclerViewAdapter mAdapter;
    List<FlightDetailsModel> mDataList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.flight_search_fragment, container, false);
        fillView();
        return mView;
    }

    private void fillView() {
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mDataList = new ArrayList<>();
        mAdapter = new RecyclerViewAdapter(getContext(), mDataList);
        mRecyclerView.setAdapter(mAdapter);

        loadData();
    }

    private void loadData() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://www.mocky.io/v2/5979c6731100001e039edcb3")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                try {
                    String responseData = response.body().string();
                    JSONObject json = new JSONObject(responseData);
                    if(json != null){
                        JSONArray flights = json.optJSONArray("flights");
                        JSONObject appendix =json.optJSONObject("appendix");

                        if(flights != null) {
                            for (int i = 0; i < flights.length(); i++) {
                                FlightDetailsModel flightModel = new FlightDetailsModel((JSONObject) flights.get(i));
                                if (mDataList == null) {
                                    mDataList = new ArrayList<>();
                                }
                                mDataList.add(flightModel);
                            }
                            if (isAdded()) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                    }
                    Log.d("response", responseData);
                } catch (JSONException e) {

                }
            }
        });
    }


}
