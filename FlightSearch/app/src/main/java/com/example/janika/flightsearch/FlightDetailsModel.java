package com.example.janika.flightsearch;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by janika on 07/08/17.
 */

public class FlightDetailsModel {

    private String originCode;
    private String destCode;
    private long departureTime;
    private long arrivalTime;
    private JSONArray fareArrays;
    private String airlines;
    private String classType;


    public FlightDetailsModel(JSONObject object){
        if(object == null) return;

        originCode = object.optString("originCode");
        destCode = object.optString("destinationCode");
        departureTime = object.optLong("departureTime");
        arrivalTime = object.optLong("arrivalTime");
        fareArrays = object.optJSONArray("fares");
        airlines = object.optString("airlineCode");
        classType = object.optString("class");
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public void setDestCode(String destCode) {
        this.destCode = destCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public JSONArray getFareArrays() {
        return fareArrays;
    }

    public void setFareArrays(JSONArray fareArrays) {
        this.fareArrays = fareArrays;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }
}
