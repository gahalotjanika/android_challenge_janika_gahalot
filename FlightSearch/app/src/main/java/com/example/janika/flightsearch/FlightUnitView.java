package com.example.janika.flightsearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author  janika on 07/08/17.
 */

public class FlightUnitView extends RecyclerView.ViewHolder{

    private TextView origin, destination;
    private TextView originTime, destinationTime;
    private TextView airlines;
    private LinearLayout parentLayout;

    public FlightUnitView(View itemView) {
        super(itemView);
        origin = (TextView) itemView.findViewById(R.id.origin);
        destination = (TextView) itemView.findViewById(R.id.destination);
        originTime = (TextView) itemView.findViewById(R.id.origin_time);
        destinationTime = (TextView) itemView.findViewById(R.id.dest_time);
        airlines = (TextView) itemView.findViewById(R.id.airlines);
        parentLayout = (LinearLayout) itemView.findViewById(R.id.parent_layout);

    }

    public void fill(FlightDetailsModel flightDetails, Context context) {
        if(flightDetails == null) return;

        origin.setText(flightDetails.getOriginCode());
        destination.setText(flightDetails.getDestCode());
        Date dateOrigin = new Date(flightDetails.getDepartureTime());
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String departureTime = formatter.format(dateOrigin);
        originTime.setText(departureTime);
        Date dateDest = new Date(flightDetails.getArrivalTime());
        String arrivalTime = formatter.format(dateDest);
        destinationTime.setText(arrivalTime);

        airlines.setText(flightDetails.getAirlines());

        JSONArray fareArray = flightDetails.getFareArrays();
        if(fareArray != null){
            for(int i=0; i < fareArray.length(); i++){
                FlightProviderView flightProviderView = new FlightProviderView(context);
               parentLayout.addView(flightProviderView);
                try {
                    flightProviderView.fillView((JSONObject) fareArray.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
