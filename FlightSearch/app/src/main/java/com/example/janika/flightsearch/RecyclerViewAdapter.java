package com.example.janika.flightsearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * @author janika on 07/08/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter {

    Context mContext;
    List<FlightDetailsModel> data;

    public RecyclerViewAdapter(Context context, List<FlightDetailsModel> flightDetailsModelList){
        this.mContext = context;
        this.data = flightDetailsModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.flight_unit_view, parent, false);
        RecyclerView.ViewHolder viewHolder = new FlightUnitView(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((FlightUnitView) holder).fill(data.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
